/**
 * @format
 * @flow strict-local
 */

import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  StyleSheet,
  DeviceEventEmitter,
  TouchableOpacity,
  Modal,
  ScrollView,
  SafeAreaView,
  NativeEventEmitter,
  Platform,
} from 'react-native';
import {MyCustomModule} from './Utils';

const {initReader, getReaderList, selectDevice} = MyCustomModule;
const eventEmitter =
  Platform.OS === 'ios' ? new NativeEventEmitter(MyCustomModule) : undefined;

const App = () => {
  const logs = useRef('Hello');
  const [output, setOutput] = useState(logs.current);
  const [selectedDevice, setSelectedDevice] = useState('Select Device');
  const [modalVisible, setModalVisible] = useState(false);
  const [deviceList, setDeviceList] = useState([]);

  useEffect(() => {
    (Platform.OS === 'ios' ? eventEmitter : DeviceEventEmitter).addListener(
      'log',
      log => {
        console.log(JSON.stringify(log));
        logs.current += '\n' + log;
        setOutput(logs.current);
      },
    );
    initReader();
    // Result from iOS is array. So, no parsing is need. Result from android is string.
    getReaderList(list =>
      setDeviceList(Platform.OS === 'ios' ? list : JSON.parse(list)),
    );
  }, []);

  const selectDeviceHandler = (device, index) => {
    setSelectedDevice(device?.name);
    setModalVisible(false);
    selectDevice(index);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.deviceSelectorContainer}>
        <TouchableOpacity
          style={styles.deviceSelectorButton}
          onPress={() => setModalVisible(true)}>
          <Text>{selectedDevice}</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.paddingHorizontal25}>
        <Text>Logs:</Text>
        <ScrollView contentContainerStyle={styles.marginLeft5}>
          <Text>{output}</Text>
        </ScrollView>
      </View>
      <Modal visible={modalVisible} transparent animationType="fade">
        <View style={styles.modalContainer}>
          <View style={styles.deviceListContainer}>
            {deviceList?.map?.((device, index) => (
              <TouchableOpacity
                key={`${index}`}
                onPress={() => selectDeviceHandler(device, index)}
                style={styles.deviceSelectorButton}>
                <Text>{device?.name}</Text>
              </TouchableOpacity>
            ))}
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#F5F5F5'},
  deviceSelectorContainer: {alignItems: 'flex-end', padding: 20},
  deviceSelectorButton: {
    borderWidth: 1,
    borderColor: 'gray',
    padding: 5,
    borderRadius: 3,
    marginBottom: 5,
  },
  modalContainer: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  deviceListContainer: {
    padding: 20,
    borderRadius: 10,
    backgroundColor: '#F5F5F5',
  },
  paddingHorizontal25: {paddingHorizontal: 25},
  marginLeft5: {marginLeft: 5},
});

export default App;
