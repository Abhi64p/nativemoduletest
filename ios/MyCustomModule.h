//
//  MyCustomModule.h
//  test
//
//  Created by Abhijith P on 06/09/21.
//

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#import <TSLAsciiCommands/TSLAsciiCommands.h>

@interface MyCustomModule : RCTEventEmitter <RCTBridgeModule, TSLBarcodeCommandBarcodeReceivedDelegate>
@end
