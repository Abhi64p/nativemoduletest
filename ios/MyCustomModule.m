//
//  MyCustomModule.m
//  test
//
//  Created by Abhijith P on 06/09/21.
//

#import <TSLAsciiCommands/TSLAsciiCommands.h>
#import <TSLAsciiCommands/TSLBinaryEncoding.h>

#import "MyCustomModule.h"

@implementation MyCustomModule

TSLAsciiCommander * _commander;
TSLBarcodeCommand *_barcodeResponder;
NSObject * _selectedReader;

- (NSArray<NSString *> *)supportedEvents {
  return @[@"log", @"barcode"];
}

- (void)barcodeReceived:(NSString *)data
{
  [self sendEventWithName:@"log" body:data];
}

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(initReader){
  [self sendEventWithName:@"log" body:@"Hello from iOS, initializing"];
  _commander = [[TSLAsciiCommander alloc] init];
  [_commander addSynchronousResponder];

  [[EAAccessoryManager sharedAccessoryManager] registerForLocalNotifications];
  
  _barcodeResponder = [[TSLBarcodeCommand alloc] init];
  _barcodeResponder.barcodeReceivedDelegate = self;
  _barcodeResponder.captureNonLibraryResponses = YES;

  [_commander addResponder:_barcodeResponder];
  [self sendEventWithName:@"log" body:@"Initiated"];
}

RCT_EXPORT_METHOD(getReaderList:(RCTResponseSenderBlock)callback){
  NSArray * _accessoryList = [[EAAccessoryManager sharedAccessoryManager] connectedAccessories];
  callback(@[_accessoryList]);
}

RCT_EXPORT_METHOD(selectDevice:(int)index){
  NSArray * _accessoryList = [[EAAccessoryManager sharedAccessoryManager] connectedAccessories];
  [_commander connect:_accessoryList[index]];
  [self sendEventWithName:@"log" body:@"Connected."];
}

@end
