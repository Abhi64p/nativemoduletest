package com.test;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.ReactPackage;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.uimanager.ViewManager;
import com.uk.tsl.rfid.asciiprotocol.AsciiCommander;
import com.uk.tsl.rfid.asciiprotocol.commands.BarcodeCommand;
import com.uk.tsl.rfid.asciiprotocol.device.ObservableReaderList;
import com.uk.tsl.rfid.asciiprotocol.device.Reader;
import com.uk.tsl.rfid.asciiprotocol.device.ReaderManager;
import com.uk.tsl.rfid.asciiprotocol.enumerations.TriState;
import com.uk.tsl.rfid.asciiprotocol.responders.IBarcodeReceivedDelegate;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MyAppPackage implements ReactPackage {
    @Override
    @NonNull
    public List<ViewManager> createViewManagers(@NonNull ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }

    @Override
    @NonNull
    public List<NativeModule> createNativeModules(@NonNull ReactApplicationContext reactContext) {
        List<NativeModule> modules = new ArrayList<>();
        modules.add(new MyCustomModule(reactContext));
        return modules;
    }
}

class MyCustomModule extends ReactContextBaseJavaModule {
    private DeviceEventManagerModule.RCTDeviceEventEmitter eventEmitter;
    private AsciiCommander commander;

    MyCustomModule(ReactApplicationContext context) {
        super(context);
    }

    @Override
    @NonNull
    public String getName() {
        return "MyCustomModule";
    }

    @ReactMethod
    public void initReader() {
        ReactContext context = getReactApplicationContext();
        eventEmitter = context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class);
        eventEmitter.emit("log", "Initializing");

        AsciiCommander.createSharedInstance(context);
        commander = AsciiCommander.sharedInstance();
        commander.clearResponders();
        commander.addSynchronousResponder();

        ReaderManager.create(context);
        ReaderManager.sharedInstance().updateList();

        BarcodeCommand barcodeResponder = new BarcodeCommand();
        barcodeResponder.setCaptureNonLibraryResponses(true);
        barcodeResponder.setUseEscapeCharacter(TriState.YES);
        barcodeResponder.setBarcodeReceivedDelegate(new IBarcodeReceivedDelegate() {
            @Override
            public void barcodeReceived(String barcode) {
                eventEmitter.emit("log", barcode);
            }
        });
        commander.addResponder(barcodeResponder);

        eventEmitter.emit("log", "Initiated");
    }

    @ReactMethod
    public void getReaderList(Callback callback) {
        ReaderManager.sharedInstance().updateList();
        List<Reader> readerList = ReaderManager.sharedInstance().getReaderList().list();
        JSONArray devices = new JSONArray();
        for (Reader reader : readerList) {
            JSONObject device = new JSONObject();
            try {
                device.put("name", reader.getDisplayName());
            } catch (Exception ex) {
                eventEmitter.emit("log", ex.getMessage());
            }
            devices.put(device);
        }
        callback.invoke(devices.toString());
    }

    @ReactMethod
    public void selectDevice(int index) {
        try {
            List<Reader> readerList = ReaderManager.sharedInstance().getReaderList().list();
            Reader reader = readerList.get(index);
            commander.setReader(reader);
            reader.connect();
            eventEmitter.emit("log", "Connected to " + reader.getDisplayName());
        } catch (Exception e) {
            eventEmitter.emit("log", e.getMessage());
        }
    }
}